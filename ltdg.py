import os
import math
from scipy.fftpack import fft, ifft, rfft, fftfreq, fftshift, rfftfreq
import numpy as np
import numpy as arange

#from ROOT import *
#import ROOT

from pylab import *
import matplotlib.pyplot as plt
import matplotlib.pyplot as pyplot
import pylab


PREFIX = "/home/kimwootae/work/LT/data"

#gROOT.Reset()
#gROOT.ForceStyle()
#color = ['ROOT.kBlue']

array1 = []
array2 = []

with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_FFT.txt"), "r") as f:
    for line in f.readlines():
        array1.append([float(e1) for e1 in line.split()])
        
with open(os.path.join(PREFIX, "20170525", "FLLnoise" , "FLLnoise_FFT.txt"), "r") as f:
    for line in f.readlines():
        array2.append([float(e1) for e1 in line.split()])

array1 = np.asarray(array1)
array2 = np.asarray(array2)

n = array1.shape[0]
print(array)
print(n)

yf1 = array1[:,1]
print(yf1)
xf1 = array1[:,0]
print(xf1)

yf2 = array2[:,1]
print(yf2)
xf2 = array2[:,0]
print(xf2)

pylab.figure(1)
pylab.plot(xf1,yf1)
pylab.xlabel('Frequency,Hz')
pylab.ylabel('FFT,V/Hz')
pylab.title('Ampnoise_Noise Power Spectrum')
pylab.yscale('log')
pylab.xscale('log')

pylab.figure(2)
pylab.plot(xf2,yf2)
pylab.xlabel('Frequency,Hz')
pylab.ylabel('FFT,V/Hz')
pylab.title('FLLnoise_Noise Power Spectrum')
pylab.yscale('log')
pylab.xscale('log')

pylab.show()


#g1 = ROOT.TGraph(len(xf),np.array(array[:,0]),np.array(array[:,1]))
#g1.Draw('AP')
#can1 = TCanvas("can1","ltdg",800,600)
#g1.Draw()
#can1.Print(os.path.join(PREFIX, "plot", "ltdg.root"))
'''
# Save Version 1.03noisespec
# Date: 2017-05-25 pm 7:03:43
# Length: 2000000
# Current weight: 0.032258064516129
# Number of averaged waves: 30
# Definition: RMS value in spectrum graph <s0> can be calculated from given here <s1>
# by <s0> = sqrt(<s1>) / N (for DC 0th - component)
# and by <s0>=1/sqrt(2) * sqrt(<s1>) / N (for Sin/Cos 1..N-1th - components)
#---------------------------------------------------------------------------------
#n noise_spectrum
'''
