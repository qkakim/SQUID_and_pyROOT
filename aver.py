import os
import math
from ROOT import *
from scipy.fftpack import fft, ifft, rfft, fftfreq, fftshift, rfftfreq
import numpy as np
import numpy as arange
import numpy as numpy
import ROOT

PREFIX = "/home/kimwootae/work/LT/data"

gROOT.Reset()
gROOT.ForceStyle()

h1 = TH2D('h1', 'h1', 25001, 0, 25000, 1000, 0, 0.1)
h2 = TH1D('h2', 'h2', 10000, 0, 0.1)
array = []

def movingaverage(interval, window_size):
    window= numpy.ones(int(window_size))/float(window_size)
    return numpy.convolve(interval, window, 'same')

y = np.zeros(1, dtype=float)
x = np.zeros(1, dtype=float)
q = np.zeros(1, dtype=float)
p = np.zeros(1, dtype=float)

"""Tree and branch"""
rf = ROOT.TFile("/home/kimwootae/work/LT/data/plot/averfft.root", "recreate")
rt1 = ROOT.TTree("row","tree title")
rt2 = ROOT.TTree("modify","tree title")
rt1.Branch('xf', x, 'fft_frq')
rt1.Branch('yf', y, 'fft_amp')
rt2.Branch('yf_sqrt', q, 'fft_noise_amp__fft/bandwidth')
rt2.Branch('yf_av', p, 'graph_average')

with open(os.path.join(PREFIX, "fll-noise", "C1fll-noise00000.txt"), "r") as f:
    for line in f.readlines():
        array.append([float(e1) for e1 in line.split()])
  
array = np.asarray(array)

n = array.shape[0]
#print(array)
#print(n)
yf = abs(fft(array[:,1]))
#print(yf)
timestep = 0.00001
xf = rfftfreq(n, d=timestep*2)
#print(xf)
tarray = np.column_stack((xf,yf))
#print(tarray)
z = math.sqrt(2/timestep)

yf_sqrt = yf/z
print(yf_sqrt)
yf_av = movingaverage(yf_sqrt,1000)
print(yf_av)
n_yf_av = len(yf_av)
print(n_yf_av)

farray = np.column_stack((yf_sqrt,yf_av))

for x, y in tarray:
    #print(x,y)
    #h1.Fill(x, y/z)
    h1.Fill(x, y/z)
    h2.Fill(y/z)
    #print(x, y/z)
    rt1.Fill()

for q, p in farray :
    rt2.Fill()

g1 = ROOT.TGraph(len(xf), xf, yf_av)
g1.Fit("pol2")
g1.Draw('AP')


rf.Write()
rf.Close()

can4 = TCanvas("can4","Example", 800, 600)
h1.Draw("clz")
can5 = TCanvas("can5","example", 800, 600)
h2.Draw()
can6 = TCanvas("can6","example", 800, 600)
g1.Draw()
can4.Print(os.path.join(PREFIX, "plot", "fft1.root"))
can5.Print(os.path.join(PREFIX, "plot", "fft2.root"))
can6.Print(os.path.join(PREFIX, "plot", "fft3.root"))
