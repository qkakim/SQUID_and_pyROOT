from ROOT import *

gROOT.Reset()
gROOT.ForceStyle()
color = ['ROOT.kBlue']

h1 = TH2D('h1', 'h1', 2000, -1, 1, 2000, -0.1, 0.2)
h2 = TH2D('h2', 'h2', 2000, -1, 1, 2000, -0.1, 0.2)
h3 = TH2D('h3', 'h3', 2000, -1, 1, 2000, -0.1, 0.2)

m1 = open("/home/kimwootae/work/LT/aaaa/m1.txt", 'r')
m2 = open("/home/kimwootae/work/LT/aaaa/m2.txt", 'r')
m3 = open("/home/kimwootae/work/LT/aaaa/m3.txt", 'r')

while True :
    line1 = m1.readline()
    if not line1: break
    b1 = line1.split(",")
    c1 = [float(e1) for e1 in b1]
    x1, y1 = c1
    h1.Fill(x1,y1)

while True :
    line2 = m2.readline()
    if not line2: break
    b2 = line2.split(",")
    c2 = [float(e2) for e2 in b2 ]
    x2, y2 = c2
    h2.Fill(x2,y2)
    
while True :
    line3 = m3.readline()
    if not line3: break
    b3 = line3.split(",")
    c3 = [float(e3) for e3 in b3 ]
    x3, y3 = c3
    h3.Fill(x3,y3)

    
can3 = TCanvas('can3', 'Example', 800, 600)
h1.Draw("colz")
h2.Draw("same")
h3.Draw("same")

can3.Print("/home/kimwootae/work/LT/plot/mresult.png")
can3.Print("/home/kimwootae/work/LT/plot/mresult.root")
