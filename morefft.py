import os
import glob
import sys
import errno
import math
import numpy as np
import numpy as arange
import ROOT
import numpy as numpy

from ROOT import *
from scipy.fftpack import fft, ifft, rfft, fftfreq, fftshift, rfftfreq

def movingaverage(interval, window_size):
    window= numpy.ones(int(window_size))/float(window_size)
    return numpy.convolve(interval, window, 'same')

PREFIX = "/home/kimwootae/work/LT/data"
path = '/home/kimwootae/work/LT/data/fll-noise/*.txt'
files = glob.glob(path)

gROOT.Reset()
gROOT.ForceStyle()

h1 = TH2D('h1', 'h1', 25001, 0, 25000, 1000, 0, 1)
h2 = TH1D('h1', 'h1', 10000, 0, 2)

array = []
for name in files :
    try :
        with open(name) as f:
            for line in f.readlines():
                array.append([float(e1) for e1 in line.split()])
    except IOError as exc:
        if exc.errno != errno.EISDIR: # Do not fail if a directory is found, just ignore it.
            raise # Propagate other kinds of IOError.

array = np.asarray(array)

n = array.shape[0]
print(array)
print(n)
yf = abs(fft(array[:,1]))
print(yf)
timestep = 0.00001*2
xf = rfftfreq(n, d=timestep)
print(xf)
tarray = np.column_stack((xf,yf))
print(tarray)
z = math.sqrt(2/timestep)
yf_s = yf/z

yf_av = movingaverage(yf_s,2)
print(yf_av)
n_yf_av = len(yf_av)
print(n_yf_av)


for x, y in tarray:
    #z = math.sqrt(2/timestep)
    h1.Fill(x, y)
    h2.Fill(y)

g1 = ROOT.TGraph(len(xf), xf, yf_av)
g1.Fit("pol2")
g1.Draw('AL')

can4 = TCanvas("can4","Example", 800, 600)
h1.Draw()
can4.Print(os.path.join(PREFIX, "plot", "fft1.root"))

can5 = TCanvas("can5","example", 800, 600)
h2.Draw()
can5.Print(os.path.join(PREFIX, "plot", "fft2.root"))

can6 = TCanvas("can6","example", 800, 600)
g1.Draw()
can6.Print(os.path.join(PREFIX, "plot", "fft3.root"))
