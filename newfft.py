import os
import glob
import errno
import sys
import math
from scipy.fftpack import fft, ifft, rfft, fftfreq, fftshift, rfftfreq
import numpy as np
import numpy as arange
from pylab import *
import matplotlib.pyplot as plt
import matplotlib.pyplot as pyplot
import pylab

PREFIX = "/home/kimwootae/work/LT/data"
#path = '/home/kimwootae/work/LT/data/20170525/Ampnoise/Ampnoise_raw*.txt'
#files = glob.glob(path)

array1 = []
array2 = []
array3 = []
array4 = []
array5 = []
array6 = []
array7 = []
array8 = []
array9 = []


with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000001.txt"), "r") as f:
    for line in f.readlines():
        array1.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000002.txt"), "r") as f:
    for line in f.readlines():
        array2.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000003.txt"), "r") as f:
    for line in f.readlines():
        array3.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000004.txt"), "r") as f:
    for line in f.readlines():
        array4.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000005.txt"), "r") as f:
    for line in f.readlines():
        array5.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000006.txt"), "r") as f:
    for line in f.readlines():
        array6.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000007.txt"), "r") as f:
    for line in f.readlines():
        array7.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000008.txt"), "r") as f:
    for line in f.readlines():
        array8.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000009.txt"), "r") as f:
    for line in f.readlines():
        array9.append([float(e1) for e1 in line.split()])
'''
with open(os.path.join(PREFIX, "20170525", "Ampnoise" , "Ampnoise_raw0000021.txt"), "r") as f:
    for line in f.readlines():
        array1.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "FLLnoise" , "FLLnoise_raw0000022.txt"), "r") as f:
    for line in f.readlines():
        array2.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "FLLnoise" , "FLLnoise_raw0000023.txt"), "r") as f:
    for line in f.readlines():
        array3.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "FLLnoise" , "FLLnoise_raw0000024.txt"), "r") as f:
    for line in f.readlines():
        array4.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "FLLnoise" , "FLLnoise_raw0000025.txt"), "r") as f:
    for line in f.readlines():
        array5.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "FLLnoise" , "FLLnoise_raw0000026.txt"), "r") as f:
    for line in f.readlines():
        array6.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "FLLnoise" , "FLLnoise_raw0000027.txt"), "r") as f:
    for line in f.readlines():
        array7.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "FLLnoise" , "FLLnoise_raw0000028.txt"), "r") as f:
    for line in f.readlines():
        array8.append([float(e1) for e1 in line.split()])
with open(os.path.join(PREFIX, "20170525", "FLLnoise" , "FLLnoise_raw0000029.txt"), "r") as f:
    for line in f.readlines():
        array9.append([float(e1) for e1 in line.split()])'''
'''
for name in files :
    try :
        with open(name) as f:
            for line in f.readlines():
                array.append([float(e1) for e1 in line.split()])   
    except IOError as exc :
        if exc.errno != errno.EISDIR: # Do not fail if a directory is found, just ignore it.
            raise # Propagate other kinds of IOError.
'''
array1 = np.asarray(array1)
array2 = np.asarray(array2)
array3 = np.asarray(array3)
array4 = np.asarray(array4)
array5 = np.asarray(array5)
array6 = np.asarray(array6)
array7 = np.asarray(array7)
array8 = np.asarray(array8)
array9 = np.asarray(array9)

n1 = array1.shape[0]
n2 = array2.shape[0]
n3 = array3.shape[0]
n4 = array4.shape[0]
n5 = array5.shape[0]
n6 = array6.shape[0]
n7 = array7.shape[0]
n8 = array8.shape[0]
n9 = array9.shape[0]

yf1 = abs(fft(array1))
yf2 = abs(fft(array2))
yf3 = abs(fft(array3))
yf4 = abs(fft(array4))
yf5 = abs(fft(array5))
yf6 = abs(fft(array6))
yf7 = abs(fft(array7))
yf8 = abs(fft(array8))
yf9 = abs(fft(array9))

timestep = 4000000
xff = np.arange(2000000.0)
print(n1)
xf = rfftfreq(n1, d=1./timestep)

#xf1 = rfftfreq(n1, d=1./timestep*2)
#tarray = np.column_stack((xf,yf))
#print(tarray)
z = math.sqrt(1./timestep)

yf1 = yf1/z
yf2 = yf2/z
yf3 = yf3/z
yf4 = yf4/z
yf5 = yf5/z
yf6 = yf6/z
yf7 = yf7/z
yf8 = yf8/z
yf9 = yf9/z

ty1array = np.column_stack((yf1, yf2))
vy1array = np.average(ty1array, axis=1)
ty2array = np.column_stack((vy1array, yf3))
vy2array = np.average(ty2array, axis=1)
ty3array = np.column_stack((vy2array, yf4))
vy3array = np.average(ty3array, axis=1)
ty4array = np.column_stack((vy3array, yf5))
vy4array = np.average(ty4array, axis=1)
ty5array = np.column_stack((vy4array, yf6))
vy5array = np.average(ty5array, axis=1)
ty6array = np.column_stack((vy5array, yf7))
vy6array = np.average(ty6array, axis=1)
ty7array = np.column_stack((vy6array, yf8))
vy7array = np.average(ty7array, axis=1)
ty8array = np.column_stack((vy7array, yf9))
vy8array = np.average(ty8array, axis=1)

print(vy8array)
plt.plot(xff,vy8array)

plt.xlabel('Frequency,Hz')
plt.ylabel('FFT,V/Hz')
plt.title('Noise Power Spectrum')
plt.xscale('log')
plt.yscale('log')

plt.show()
'''
pylab.figure(1)
pylab.plot(xf1,yf1)
pylab.yscale('log')
pylab.xscale('log')

pylab.figure(1)
pylab.plot(xf2,yf2)
pylab.yscale('log')
pylab.xscale('log')

pylab.figure(1)
pylab.plot(xf3,yf3)
pylab.yscale('log')
pylab.xscale('log')

pylab.figure(1)
pylab.plot(xf4,yf4)
pylab.yscale('log')
pylab.xscale('log')

pylab.figure(1)
pylab.plot(xf5,yf5)
pylab.yscale('log')
pylab.xscale('log')

pylab.figure(1)
pylab.plot(xf6,yf6)
pylab.yscale('log')
pylab.xscale('log')

pylab.figure(1)
pylab.plot(xf7,yf7)
pylab.yscale('log')
pylab.xscale('log')

pylab.figure(1)
pylab.plot(xf8,yf8)
pylab.yscale('log')
pylab.xscale('log')

pylab.figure(1)
pylab.plot(xf9,yf9)
pylab.yscale('log')
pylab.xscale('log')
'''
pylab.show()





# Save Version 1.03
# Date: 2017-05-25 pm 6:55:42
# actual sample rate: 4000000
# re-sampled rate: 4000000
# actualSamples: 2000000
# gain: 3.45949156326242E-05
# offset: -0.000101232886663638
# relativeInitialX: -0.24999989
# absouteInitialX: 5159.52558432
# xIncrement: 2.5E-07
#---------------------------------------------------------------------------------
#ch1
